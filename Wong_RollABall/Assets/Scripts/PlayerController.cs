﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
            transform.localScale += new Vector3(0.05f, 0.05f, 0.05f); // grow player to start enable scaling wall
        }
        if (other.gameObject.CompareTag("Big Pickup"))
        {
            other.gameObject.SetActive(false);
            count += 26;
            SetCountText();
        }
        if (other.gameObject.CompareTag("Up Elevator"))
        {
            transform.position += new Vector3(0.0f, 16.03f, 0.0f); // teleport directly upwards, maintaining all other atributes of transform
        }
        if (other.gameObject.CompareTag("Down Elevator"))
        {
            transform.position += new Vector3(0.0f, -16.01f, 0.0f); // teleport directly downDwards, maintaining all other atributes of transform
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 50)
        {
            winText.text = "You Win!";
        }
    }
}
