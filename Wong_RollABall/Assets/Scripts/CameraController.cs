﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public GameObject player;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position; // set original camera position from player
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offset; // maintain camera position relative to player
    }
}
