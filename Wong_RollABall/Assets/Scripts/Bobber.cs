﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : MonoBehaviour
{

    private float counter;
    private Vector3 starting;

    // Start is called before the first frame update
    void Start()
    {
        counter = 0f;
        starting = transform.position; // starting position to bob from
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(starting.x, starting.y + 0.5f * Mathf.Sin(counter / 60.0f), starting.z); // bobbing movement
        counter += 1;
    }
}
